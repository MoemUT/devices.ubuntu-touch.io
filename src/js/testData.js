module.exports = {
  runBefore: beforeElaboration,
  runAfter: afterElaboration
};

let Validator = require("./dataValidator.js");
const rules = require("../../data/validationRules.js")();

const portStatus = require("../../data/portStatus.json");
const progressStages = require("../../data/progressStages.json");

const dataUtils = require("./dataUtils.js");

const path = require("path");

function beforeElaboration(device) {
  let ciDiff = [];

  try {
    ciDiff = process.env.CI_FILES_CHANGED.split("\n").map((e) => {
      return path.basename(e, device.fileInfo.extension);
    });
  } catch (e) {}

  if (
    !process.argv.includes("test-changes") ||
    ciDiff.includes(device.fileInfo.name)
  ) {
    let validation = new Validator(device, rules);
    validation.passes();
    let notValid = validation.errors();

    console.log(
      notValid.length ? "\x1b[33m%s\x1b[0m" : "\x1b[32m%s\x1b[0m",
      "\n[" + device.fileInfo.name + "] " + device.name
    );

    notValid.forEach((err) => console.log(err));
    device.notValid = notValid;
  }
}

function afterElaboration(device) {
  if (device.portStatus) {
    nextStageRequirements(device);
  }
}

/* Helpers */
function convertToFeatureMatrix(data) {
  let output = [];
  for (let feature of data) {
    let category = output.find((el) => el.categoryName == feature.category);

    if (!category) {
      category = {
        categoryName: feature.category,
        features: []
      };
      output.push(category);
    }
    category.features.push({
      name: feature.name,
      value: "-"
    });
  }
  return output;
}

/* Functions */
function nextStageRequirements(device) {
  let missing = []; // Terminal logs

  let currentStage = device.progressStage,
    nextStage = progressStages.findIndex((el) => el == currentStage) + 1;

  dataUtils.forEachFeature(device, function (feature, category) {
    if (feature.stage == nextStage) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);
      if (graphQlFeature && graphQlFeature.value != "+") {
        missing.push({
          id: feature.id,
          name: feature.name,
          category: category.categoryName
        });
      }
    }
  });

  device.nextProgressStage = progressStages[nextStage];
  device.nextStageRequirements = convertToFeatureMatrix(missing);
}
