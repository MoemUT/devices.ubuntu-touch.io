---
name: "Xiaomi Redmi 3X, 3S and 3SP"
comment: "wip"
deviceType: "phone"
image: "http://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-3-pro-2.jpg"
maturity: .3
---

### Device specifications

|    Component | Details                                 |
| -----------: | --------------------------------------- |
|      Chipset | Qualcomm MSM8937 Snapdragon 430         |
|          CPU | Octa-core 1.3 GHz@4 1,0ghz@4 Cortex-A53 |
| Architecture | arm64                                   |
|          GPU | Adreno 505                              |
|      Display | 720x1280                                |
|      Storage | 16 GB / 32 GB                           |
|       Memory | 2 GB / 3 GB                             |
|      Cameras | 13 MP, LED flash<br>5 MP, No flash      |
|   Dimensions | 139.3 x 69.6 x 8.5 mm.                  |

### Port status

|         Component | Status | Details            |
| ----------------: | :----: | ------------------ |
|          AppArmor |   Y    |                    |
|      Boot into UI |   Y    |                    |
|            Camera |   Y    | Requires gst-droid |
|    Cellular Calls |   Y    |                    |
|     Cellular Data |   Y    |                    |
|               GPS |   Y    |                    |
|           Sensors |   Y    |                    |
|             Sound |   Y    |                    |
| UBPorts Installer |   N    |                    |
|  UBPorts Recovery |   Y    |                    |
|          Vibrator |   Y    |                    |
|             Wi-Fi |   Y    |                    |

### Maintainer(s)

t.me/DarknessHiddenorg

### Forum topic

Https://t.me/utland

### Source repos

https://github.com/Ubports-Land

### CI builds

https://github.com/Ubports-Land/Ubports-CI

### Notes

To download artifacts need login github account
