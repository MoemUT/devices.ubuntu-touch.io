---
name: "Xiaomi Redmi 9 and 9 Prime"
deviceType: "phone"
portType: "Halium 10.0"
image: "https://fdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-9-2.jpg"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "dualSim"
        value: "+"
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "?"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "-"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "?"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "rtcTime"
        value: "-"
      - id: "shutdown"
        value: "-"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "?"
      - id: "nfc"
        value: "?"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "?"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "-"
      - id: "adb"
        value: "-"
      - id: "wiredExternalMonitor"
        value: "x"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Mediatek Helio G80"
  - id: "gpu"
    value: "Mali-G52 MC2"
  - id: "rom"
    value: "32GB/64GB/128GB"
  - id: "ram"
    value: "3GB/4GB/6GB"
  - id: "android"
    value: "Android 10 Miui 11"
  - id: "battery"
    value: "5000 mAh"
  - id: "display"
    value: "IPS LCD, 400 nits (typ) 6.53 inches 1080 x 2340 pixels, 19.5:9 ratio (~395 ppi density)"
  - id: "arch"
    value: "arm64"
  - id: "rearCamera"
    value: "13MP(wide), 8MP(ultrawide), 5MP(macro), 2MP(depth)"
  - id: "frontCamera"
    value: "8MP"
  - id: "dimensions"
    value: "163.3 x 77 x 9.1 mm (6.43 x 3.03 x 0.36 in)"
  - id: "weight"
    value: "198 g (6.98 oz)"
contributors:
  - name: TheKit
    forum: "https://forums.ubports.com/user/thekit"
    photo: ""
  - name: areyoudeveloper
    forum: ""
    photo: ""
externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
    icon: "telegram"
  - name: "Device Support"
    link: "https://t.me/utlance"
    icon: "yumi"
  - name: "Report a bug"
    link: "https://gitlab.com/groups/ubports/community-ports/android10/xiaomi-redmi-9/-/issues"
    icon: "github"
  - name: "Device Source"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-redmi-9"
    icon: "github"
  - name: "Kernel source"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-redmi-9/kernel-xiaomi-mt6768"
    icon: "github"
---
