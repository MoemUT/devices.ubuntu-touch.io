/**
 * Ubuntu Touch devices website
 * Copyright (C) 2021 UBports Foundation <info@ubports.com>
 * Copyright (C) 2021 Jan Sprinz <neo@neothethird.de>
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

let rssGenerator = require("./src/js/generateRss.js");

module.exports = {
  siteName: "Ubuntu Touch • Linux Phone",
  titleTemplate: "%s • Ubuntu Touch • Linux Phone",
  siteUrl: "https://devices.ubuntu-touch.io",
  icon: "./src/assets/img/services/yumi.svg",
  images: {
    defaultQuality: 90
  },
  plugins: [
    { use: "gridsome-plugin-svg" },
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "./data/devices/*.md",
        typeName: "Device",
        pathPrefix: "/device",
        remark: {}
      }
    },
    {
      use: "@capsia/gridsome-plugin-local-image",
      options: {
        typeName: "Device",
        sourceField: "contributors.photo",
        targetPath: "./src/assets/img/remote"
      }
    },
    {
      use: "@capsia/gridsome-plugin-local-image",
      options: {
        typeName: "Device",
        sourceField: "image",
        targetPath: "./src/assets/img/remote"
      }
    },
    {
      use: "gridsome-plugin-flexsearch",
      options: {
        searchFields: ["name"],
        autoFetch: false,
        collections: [
          {
            typeName: "Device",
            indexName: "Device",
            fields: [
              "deviceType",
              "codename",
              "path",
              "name",
              "progress",
              "tag"
            ]
          }
        ]
      }
    },
    {
      use: "gridsome-plugin-matomo",
      options: {
        host: "https://analytics.ubports.com",
        siteId: 3
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        config: {
          "/device/*": {
            changefreq: "weekly",
            priority: 0.7
          }
        }
      }
    },
    {
      use: "gridsome-plugin-robots",
      options: {
        policy: [{ userAgent: "*", allow: "/" }]
      }
    },
    {
      use: "gridsome-plugin-netlify-redirects",
      options: {
        redirects: [
          {
            from: "/installer package=:package",
            to: "/installer/:package",
            status: "302!"
          },
          {
            from: "/about/categories",
            to: "https://docs.ubports.com/en/latest/porting/introduction/Intro.html",
            status: "301"
          }
        ]
      }
    },
    {
      use: "gridsome-plugin-git-history",
      options: {
        typeName: "Device",
        targetPath: "gitData",
        gitlog: {
          number: -1,
          fields: ["authorDate", "authorName", "hash"]
        }
      }
    },
    {
      use: "@microflash/gridsome-plugin-feed",
      options: {
        contentTypes: ["Device"],

        feedOptions: {
          id: "devices.ubuntu-touch.io/new",
          title: "Ubuntu Touch • Latest devices",
          description:
            "Didn't found your device in the list? Subscribe to this feed and you'll know when a new Ubuntu Touch device gets ported.",
          link: "https://devices.ubuntu-touch.io/",
          language: "en",
          image: "https://devices.ubuntu-touch.io/social-preview.jpg",
          favicon: "https://devices.ubuntu-touch.io/favicon.ico",
          copyright: "© Copyright 2021 UBports Foundation",
          feedLinks: {
            rss: "https://devices.ubuntu-touch.io/new.xml",
            atom: "https://devices.ubuntu-touch.io/new.atom"
          },
          author: {
            name: "UBports contributors",
            link: "https://ubports.com/"
          }
        },

        rss: {
          enabled: true,
          output: "/new.xml"
        },
        atom: {
          enabled: true,
          output: "/new.atom"
        },

        maxItems: 25,

        // sets the properties on each feed item
        // See https://github.com/jpmonette/feed#example for all options
        nodeToFeedItem: (node) => rssGenerator(node)
      }
    }
  ],
  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      plugins: []
    }
  },
  templates: {
    Device: [
      {
        path: "/device/:fileInfo__name",
        component: "./src/templates/Device.vue"
      },
      {
        name: "developer",
        path: "/device/:fileInfo__name/developer",
        component: "./src/templates/DeviceDevelopment.vue"
      }
    ]
  },
  permalinks: {
    slugify: {
      use: "@sindresorhus/slugify",
      options: {
        decamelize: false
      }
    }
  }
};
